package br.com.ilhasoft.ilhapay.sample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.ilhasoft.ilhapay.IlhaPay;
import br.com.ilhasoft.ilhapay.RegisterListener;
import br.com.ilhasoft.ilhapay.model.Address;
import br.com.ilhasoft.ilhapay.model.BankAccount;
import br.com.ilhasoft.ilhapay.model.CreditCard;
import br.com.ilhasoft.ilhapay.model.Customer;
import br.com.ilhasoft.ilhapay.model.Phone;
import br.com.ilhasoft.ilhapay.model.Recipient;
import br.com.ilhasoft.ilhapay.model.SecureCreditCard;
import br.com.ilhasoft.ilhapay.model.SplitRule;
import br.com.ilhasoft.ilhapay.model.Transaction;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final String API_KEY = "ak_test_07CnAQEsgWUneqS7htBLjDxBVlq5Sp";
    private static final String ENCRYPTION_KEY = "ek_test_KdT2DOaqJVGSiIZqrEuoMmuaodw4a5";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button saveCard = (Button) findViewById(R.id.saveCard);
        saveCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                encryptCard();
            }
        });

        Button saveRecipient = (Button) findViewById(R.id.saveRecipient);
        saveRecipient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveRecipient();
            }
        });

        Button sendTransaction = (Button) findViewById(R.id.sendTransaction);
        sendTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendTransaction();
            }
        });

        Button refundTransaction = (Button) findViewById(R.id.refundTransaction);
        refundTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refundTransaction();
            }
        });
    }

    private void refundTransaction() {
        IlhaPay ilhaPay = new IlhaPay(this)
                .apiKey(API_KEY);

        ilhaPay.sendTransactionRefund(534103, new RegisterListener<Transaction>() {
            @Override
            public void onRegistered(Transaction value) {
                Log.d(TAG, "onRegistered() called with: " + "value = [" + value + "]");
            }

            @Override
            public void onError(Throwable throwable, String errorMessage) {
                Log.d(TAG, "onError() called with: " + "throwable = [" + throwable + "], errorMessage = [" + errorMessage + "]");
            }
        });
    }

    private void sendTransaction() {
        IlhaPay ilhaPay = new IlhaPay(this)
                .apiKey(API_KEY);

        Calendar calendar = Calendar.getInstance();
        calendar.roll(Calendar.YEAR, -30);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        Customer customer = new Customer();
        customer.setBornAt(simpleDateFormat.format(calendar.getTime()));
        customer.setEmail("johndcc@gmail.com");
        customer.setName("John Cordeiro");
        customer.setSex(Customer.MALE);
        customer.setDocumentNumber("07368271451");

        Phone phone = new Phone("82", "999489287");
        customer.setPhone(phone);

        Address address = new Address();
        address.setStreet("Rua Professor Nabuco Lopes");
        address.setStreetNumber("159");
        address.setComplementary("Apto 304");
        address.setNeighborhood("Jatiúca");
        address.setZipcode("57036730");

        customer.setAddress(address);

        Transaction transaction = new Transaction();
        transaction.setCustomer(customer);
        transaction.setApiKey(API_KEY);
        transaction.setAmount(100);
        transaction.setCapture(false);
        transaction.setCardId("card_cippqtoua00an3i6drd72xbsp");

        Map<String, String> metadata = new HashMap<>();
        metadata.put("lesson", "FCIGE922nH");
        transaction.setMetadata(metadata);

        SplitRule splitRule1 = new SplitRule();
        splitRule1.setPercentage(70f);
        splitRule1.setChargeProcessingFee(true);
        splitRule1.setRecipientId("re_cipq0xg6100friu6eqmjiwa7i");

        SplitRule splitRule2 = new SplitRule();
        splitRule2.setPercentage(30f);
        splitRule2.setChargeProcessingFee(true);
        splitRule2.setRecipientId("re_cipq0xg6100friu6eqmjiwa7i");

        List<SplitRule> splitRules = new ArrayList<>();
        splitRules.add(splitRule1);
        splitRules.add(splitRule2);
        transaction.setSplitRules(splitRules);

        ilhaPay.sendTransaction(transaction, new RegisterListener<Transaction>() {
            @Override
            public void onRegistered(Transaction value) {
                Log.d(TAG, "onRegistered() called with: " + "value = [" + value + "]");
            }

            @Override
            public void onError(Throwable throwable, String errorMessage) {
                Log.d(TAG, "onError() called with: " + "throwable = [" + throwable + "], errorMessage = [" + errorMessage + "]");
            }
        });
    }

    private void saveRecipient() {
        IlhaPay ilhaPay = new IlhaPay(this)
                .apiKey(API_KEY);

        BankAccount account = new BankAccount();
        account.setAgency("33324");
        account.setBankCode("104");
        account.setBankAccount("44978");
        account.setBankAccountDv("4");
        account.setDocumentNumber("073.682.714-51");
        account.setLegalName("John Dalton Costa Cordeiro");

        Recipient recipient = new Recipient();
        recipient.setAccount(account);
        recipient.setTransferEnabled(true);
        recipient.setTransferDay(5);
        recipient.setTransferInterval(Recipient.TRANSFER_MONTHLY);

        ilhaPay.registerRecipient(recipient, new RegisterListener<Recipient>() {
            @Override
            public void onRegistered(Recipient value) {
                Log.d(TAG, "onRegistered() called with: " + "value = [" + value + "]");
            }

            @Override
            public void onError(Throwable throwable, String errorMessage) {
                Log.d(TAG, "onError() called with: " + "throwable = [" + throwable + "], errorMessage = [" + errorMessage + "]");
            }
        });
    }

    private void encryptCard() {
        IlhaPay ilhaPay = new IlhaPay(this)
                .encryptionKey(ENCRYPTION_KEY)
                .apiKey(API_KEY);

        CreditCard creditCard = new CreditCard()
                .setFullCardNumber("4984 2350 3422 5734")
                .setHolderName("JOHN D C CORDEIRO")
                .setExpirationMonth("09")
                .setExpirationYear("16")
                .setVerificationCode("260");

        ilhaPay.registerCard(creditCard, new RegisterListener<SecureCreditCard>() {
            @Override
            public void onRegistered(SecureCreditCard secureCreditCard) {
                Log.d(TAG, "onRegistered() called with: " + "secureCreditCard = [" + secureCreditCard + "]");
            }

            @Override
            public void onError(Throwable throwable, String errorMessage) {
                Log.d(TAG, "onError() called with: " + "throwable = [" + throwable + "], errorMessage = [" + errorMessage + "]");
            }
        });
    }
}
