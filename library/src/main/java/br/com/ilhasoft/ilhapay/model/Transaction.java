package br.com.ilhasoft.ilhapay.model;

import android.support.annotation.StringDef;

import com.google.gson.annotations.SerializedName;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;
import java.util.Map;

/**
 * Created by john-mac on 6/21/16.
 */
public class Transaction {

    public static final String STATUS_REASON_ACQUIRER = "acquirer";
    public static final String STATUS_REASON_ANTIFRAUD = "antifraud";
    public static final String STATUS_REASON_INTERNAL_ERROR = "internal_error";
    public static final String STATUS_REASON_NO_ACQUIRER = "no_acquirer";
    public static final String STATUS_REASON_ACQUIRER_TIMEOUT = "acquirer_timeout";

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({STATUS_REASON_ACQUIRER, STATUS_REASON_ACQUIRER_TIMEOUT, STATUS_REASON_ANTIFRAUD
    , STATUS_REASON_INTERNAL_ERROR, STATUS_REASON_NO_ACQUIRER})
    public @interface StatusReason{}

    public static final String STATUS_PAID = "paid";
    public static final String STATUS_PROCESSING = "processing";
    public static final String STATUS_AUTHORIZED = "authorized";
    public static final String STATUS_WAITING_PAYMENT = "waiting_payment";
    public static final String STATUS_REFUNDED = "refunded";
    public static final String STATUS_PENDING_REFUND = "pending_refund";
    public static final String STATUS_REFUSED = "refused";

    @StringDef({STATUS_PROCESSING, STATUS_AUTHORIZED, STATUS_PAID, STATUS_REFUNDED
            , STATUS_WAITING_PAYMENT, STATUS_PENDING_REFUND, STATUS_REFUSED})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Status{}

    @SerializedName("id")
    private Integer id;

    @SerializedName("api_key")
    private String apiKey;

    @Status
    @SerializedName("status")
    private String status;

    @SerializedName("refuse_reason")
    private String refuseReason;

    @StatusReason
    @SerializedName("status_reason")
    private String statusReason;

    @SerializedName("amount")
    private Integer amount;

    @SerializedName("card_id")
    private String cardId;

    @SerializedName("capture")
    private Boolean capture;

    @SerializedName("metadata")
    private Map<String, String> metadata;

    @SerializedName("split_rules")
    private List<SplitRule> splitRules;

    @SerializedName("customer")
    private Customer customer;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @Status
    public String getStatus() {
        return status;
    }

    public void setStatus(@Status String status) {
        this.status = status;
    }

    public String getRefuseReason() {
        return refuseReason;
    }

    public void setRefuseReason(String refuseReason) {
        this.refuseReason = refuseReason;
    }

    @StatusReason
    public String getStatusReason() {
        return statusReason;
    }

    public void setStatusReason(@StatusReason String statusReason) {
        this.statusReason = statusReason;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Boolean getCapture() {
        return capture;
    }

    public void setCapture(Boolean capture) {
        this.capture = capture;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, String> metadata) {
        this.metadata = metadata;
    }

    public List<SplitRule> getSplitRules() {
        return splitRules;
    }

    public void setSplitRules(List<SplitRule> splitRules) {
        this.splitRules = splitRules;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
