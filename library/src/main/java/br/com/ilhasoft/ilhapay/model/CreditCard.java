package br.com.ilhasoft.ilhapay.model;

/**
 * Created by john-mac on 6/16/16.
 */
public class CreditCard {

    private String fullCardNumber;

    private String holderName;

    private String expirationMonth;

    private String expirationYear;

    private String verificationCode;

    public String getFullCardNumber() {
        return fullCardNumber;
    }

    public CreditCard setFullCardNumber(String fullCardNumber) {
        this.fullCardNumber = fullCardNumber;
        return this;
    }

    public String getHolderName() {
        return holderName;
    }

    public CreditCard setHolderName(String holderName) {
        this.holderName = holderName;
        return this;
    }

    public String getExpirationMonth() {
        return expirationMonth;
    }

    public CreditCard setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
        return this;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public CreditCard setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
        return this;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public CreditCard setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
        return this;
    }
}
