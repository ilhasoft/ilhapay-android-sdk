package br.com.ilhasoft.ilhapay.network;

import java.util.List;

/**
 * Created by john-mac on 6/16/16.
 */
public class ErrorResponse {

    private List<ErrorResponseMessage> errors;

    private String url;

    private String method;

    public List<ErrorResponseMessage> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorResponseMessage> errors) {
        this.errors = errors;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }
}
