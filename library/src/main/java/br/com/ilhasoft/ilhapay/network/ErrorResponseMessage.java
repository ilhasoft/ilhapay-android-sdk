package br.com.ilhasoft.ilhapay.network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by john-mac on 6/16/16.
 */
public class ErrorResponseMessage {

    private String type;

    @SerializedName("parameter_name")
    private String parameterName;

    private String message;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
