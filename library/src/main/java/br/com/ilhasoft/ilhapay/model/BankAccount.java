package br.com.ilhasoft.ilhapay.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by john-mac on 6/21/16.
 */
public class BankAccount {

    private String id;

    @SerializedName("bank_code")
    private String bankCode;

    @SerializedName("agencia")
    private String agency;

    @SerializedName("agencia_dv")
    private String agencyDv;

    @SerializedName("conta")
    private String bankAccount;

    @SerializedName("conta_dv")
    private String bankAccountDv;

    @SerializedName("document_type")
    private String documentType;

    @SerializedName("document_number")
    private String documentNumber;

    @SerializedName("legal_name")
    private String legalName;

    @SerializedName("charge_transfer_fees")
    private Boolean chargeTransferFees;

    @SerializedName("date_created")
    private Date dateCreated;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAgency() {
        return agency;
    }

    public void setAgency(String agency) {
        this.agency = agency;
    }

    public String getAgencyDv() {
        return agencyDv;
    }

    public void setAgencyDv(String agencyDv) {
        this.agencyDv = agencyDv;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankAccountDv() {
        return bankAccountDv;
    }

    public void setBankAccountDv(String bankAccountDv) {
        this.bankAccountDv = bankAccountDv;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    public Boolean getChargeTransferFees() {
        return chargeTransferFees;
    }

    public void setChargeTransferFees(Boolean chargeTransferFees) {
        this.chargeTransferFees = chargeTransferFees;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }
}
