package br.com.ilhasoft.ilhapay.model;

import android.support.annotation.StringDef;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by john-mac on 6/21/16.
 */
public class Recipient {

    public static final String TRANSFER_DAILY = "daily";
    public static final String TRANSFER_WEEKLY = "weekly";
    public static final String TRANSFER_MONTHLY = "monthly";

    @StringDef({TRANSFER_DAILY, TRANSFER_MONTHLY, TRANSFER_WEEKLY})
    public @interface TransferInterval{}

    @SerializedName("api_key")
    private String apiKey;

    private String id;

    @SerializedName("bank_account")
    private BankAccount account;

    @SerializedName("transfer_enabled")
    private Boolean transferEnabled;

    @SerializedName("transfer_interval")
    private String transferInterval;

    @SerializedName("transfer_day")
    private Integer transferDay;

    @SerializedName("automatic_anticipation_enabled")
    private Boolean automaticAnticipationEbabled;

    @SerializedName("anticipatable_volume_percentage")
    private Integer ancitipatableVolumePercentage;

    @SerializedName("date_created")
    private Date dateCreated;

    @SerializedName("date_updated")
    private Date dateUpdated;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BankAccount getAccount() {
        return account;
    }

    public void setAccount(BankAccount account) {
        this.account = account;
    }

    public Boolean getTransferEnabled() {
        return transferEnabled;
    }

    public void setTransferEnabled(Boolean transferEnabled) {
        this.transferEnabled = transferEnabled;
    }

    @TransferInterval
    public String getTransferInterval() {
        return transferInterval;
    }

    public void setTransferInterval(@TransferInterval String transferInterval) {
        this.transferInterval = transferInterval;
    }

    public Integer getTransferDay() {
        return transferDay;
    }

    public void setTransferDay(Integer transferDay) {
        this.transferDay = transferDay;
    }

    public Boolean getAutomaticAnticipationEbabled() {
        return automaticAnticipationEbabled;
    }

    public void setAutomaticAnticipationEbabled(Boolean automaticAnticipationEbabled) {
        this.automaticAnticipationEbabled = automaticAnticipationEbabled;
    }

    public Integer getAncitipatableVolumePercentage() {
        return ancitipatableVolumePercentage;
    }

    public void setAncitipatableVolumePercentage(Integer ancitipatableVolumePercentage) {
        this.ancitipatableVolumePercentage = ancitipatableVolumePercentage;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
}
