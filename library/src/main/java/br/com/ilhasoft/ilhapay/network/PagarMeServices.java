package br.com.ilhasoft.ilhapay.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.ilhasoft.ilhapay.BuildConfig;
import br.com.ilhasoft.ilhapay.model.Recipient;
import br.com.ilhasoft.ilhapay.model.SecureCreditCard;
import br.com.ilhasoft.ilhapay.model.Transaction;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by john-mac on 6/15/16.
 */
public class PagarMeServices {

    private static final String BASE_URL = "https://api.pagar.me/1/";

    private final PagarMeApi api;
    private final Retrofit retrofit;

    public PagarMeServices() {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }

        final Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .create();

        retrofit = new Retrofit.Builder()
                .client(builder.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();

        api = retrofit.create(PagarMeApi.class);
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public Call<CardHashResponse> getCardHashKey(String encryptionKey) {
        return api.getCardHashKey(encryptionKey);
    }

    public Call<SecureCreditCard> postCard(String apiKey, String cardHash) {
        return api.postCard(apiKey, cardHash);
    }

    public Call<Recipient> postRecipient(String apiKey, Recipient recipient) {
        recipient.setApiKey(apiKey);
        return api.postRecipient(recipient);
    }

    public Call<Recipient> putRecipient(String apiKey, Recipient recipient) {
        recipient.setApiKey(apiKey);
        return api.putRecipient(recipient.getId(), recipient);
    }

    public Call<Transaction> postTransaction(String apiKey, Transaction transaction) {
        transaction.setApiKey(apiKey);
        return api.postTransaction(transaction);
    }

    public Call<Transaction> postTransactionRefund(String apiKey, Integer transactionId) {
        return api.postTransactionRefund(apiKey, transactionId);
    }

}
