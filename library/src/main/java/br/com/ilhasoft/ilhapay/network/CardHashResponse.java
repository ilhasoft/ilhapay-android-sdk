package br.com.ilhasoft.ilhapay.network;

import com.google.gson.annotations.SerializedName;

/**
 * Created by john-mac on 6/15/16.
 */
public class CardHashResponse {

    private String id;

    @SerializedName("public_key")
    private String publicKey;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }
}
