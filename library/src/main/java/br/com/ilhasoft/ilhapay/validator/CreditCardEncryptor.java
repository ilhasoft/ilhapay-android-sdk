package br.com.ilhasoft.ilhapay.validator;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Base64;

import org.spongycastle.jce.provider.BouncyCastleProvider;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyFactory;
import java.security.Security;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import br.com.ilhasoft.ilhapay.model.CreditCard;

/**
 * Class to generate card hash to transmit between http requests
 *
 * @author John Cordeiro
 */
public class CreditCardEncryptor {

    /**
     * Generate the RSA based encryption of a credit card using a public key
     *
     * @return hash of the credit card encrypted
     * @throws Exception
     */
    public static String encrypt(CreditCard creditCard, String publicKey) throws Exception {
        if (!inEncryptDataValid(creditCard, publicKey)) {
            throw new IllegalStateException("Credit card data is incomplete");
        }

        Security.addProvider(new BouncyCastleProvider());

        Cipher cipher;
        try {
            cipher = Cipher.getInstance("RSA/None/PKCS1Padding");
        } catch (Exception exception) {
            cipher = Cipher.getInstance("RSA");
        }

        StringBuilder content = extractKeyContent(publicKey);

        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, keyFactory.generatePublic(new X509EncodedKeySpec(Base64.decode(content.toString(), Base64.DEFAULT))));
        byte[] cipherText = cipher.doFinal(createHash(creditCard).getBytes());

        return Base64.encodeToString(cipherText, Base64.DEFAULT);
    }

    private static boolean inEncryptDataValid(CreditCard creditCard, String publicKey) {
        return !TextUtils.isEmpty(creditCard.getFullCardNumber())
        && !TextUtils.isEmpty(creditCard.getHolderName())
        && !TextUtils.isEmpty(creditCard.getExpirationMonth())
        && !TextUtils.isEmpty(creditCard.getExpirationYear())
        && !TextUtils.isEmpty(creditCard.getVerificationCode())
        && !TextUtils.isEmpty(publicKey);
    }

    @NonNull
    private static StringBuilder extractKeyContent(String publicKey) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(publicKey.getBytes("UTF-8"));
        BufferedReader pemReader = new BufferedReader(new InputStreamReader(byteArrayInputStream));

        StringBuilder content = new StringBuilder();
        String line;
        while ((line = pemReader.readLine()) != null) {
            if (line.contains("-----BEGIN PUBLIC KEY-----")) {
                while ((line = pemReader.readLine()) != null) {
                    if (line.contains("-----END PUBLIC KEY")) {
                        break;
                    }
                    content.append(line.trim());
                }
                break;
            }
        }
        return content;
    }

    private static String createHash(CreditCard creditCard) {
        String year = creditCard.getExpirationYear();
        year = year.length() > 2 ? year.substring(year.length()-2) : year;

        String month = creditCard.getExpirationMonth();
        month = month.length() < 2 ? "0" + month : month;

        Uri uri = new Uri.Builder()
                .appendQueryParameter("card_number", creditCard.getFullCardNumber())
                .appendQueryParameter("card_holder_name", creditCard.getHolderName())
                .appendQueryParameter("card_expiration_date", month+year)
                .appendQueryParameter("card_cvv", creditCard.getVerificationCode())
                .build();
        return uri.getQuery();
    }
}
