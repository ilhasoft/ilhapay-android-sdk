package br.com.ilhasoft.ilhapay.network;

import br.com.ilhasoft.ilhapay.model.Recipient;
import br.com.ilhasoft.ilhapay.model.SecureCreditCard;
import br.com.ilhasoft.ilhapay.model.Transaction;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by john-mac on 6/15/16.
 */
interface PagarMeApi {

    @GET("transactions/card_hash_key")
    Call<CardHashResponse> getCardHashKey(@Query("encryption_key") String encriptionKey);

    @FormUrlEncoded
    @POST("cards")
    Call<SecureCreditCard> postCard(@Field("api_key") String apiKey, @Field("card_hash") String cardHash);

    @POST("recipients")
    Call<Recipient> postRecipient(@Body Recipient recipient);

    @PUT("recipients/{id}")
    Call<Recipient> putRecipient(@Path("id") String id, @Body Recipient recipient);

    @POST("transactions")
    Call<Transaction> postTransaction(@Body Transaction transaction);

    @FormUrlEncoded
    @POST("transactions/{id}/refund")
    Call<Transaction> postTransactionRefund(@Field("api_key") String apiKey, @Path("id") Integer id);

}
