package br.com.ilhasoft.ilhapay.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by john-mac on 6/21/16.
 */
public class Address {

    @SerializedName("street")
    private String street;

    @SerializedName("street_number")
    private String streetNumber;

    @SerializedName("complementary")
    private String complementary;

    @SerializedName("neighborhood")
    private String neighborhood;

    @SerializedName("zipcode")
    private String zipcode;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getComplementary() {
        return complementary;
    }

    public void setComplementary(String complementary) {
        this.complementary = complementary;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }
}
