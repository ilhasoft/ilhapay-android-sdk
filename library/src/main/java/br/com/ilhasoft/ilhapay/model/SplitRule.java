package br.com.ilhasoft.ilhapay.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by john-mac on 6/21/16.
 */
public class SplitRule {

    @SerializedName("recipient_id")
    private String recipientId;

    @SerializedName("charge_processing_fee")
    private Boolean chargeProcessingFee;

    @SerializedName("liable")
    private Boolean liable;

    @SerializedName("percentage")
    private Float percentage;

    @SerializedName("amount")
    private Float amount;

    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public Boolean getChargeProcessingFee() {
        return chargeProcessingFee;
    }

    public void setChargeProcessingFee(Boolean chargeProcessingFee) {
        this.chargeProcessingFee = chargeProcessingFee;
    }

    public Boolean getLiable() {
        return liable;
    }

    public void setLiable(Boolean liable) {
        this.liable = liable;
    }

    public Float getPercentage() {
        return percentage;
    }

    public void setPercentage(Float percentage) {
        this.percentage = percentage;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }
}
