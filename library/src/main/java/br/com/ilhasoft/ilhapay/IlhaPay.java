package br.com.ilhasoft.ilhapay;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import br.com.ilhasoft.ilhapay.model.CreditCard;
import br.com.ilhasoft.ilhapay.model.Recipient;
import br.com.ilhasoft.ilhapay.model.SecureCreditCard;
import br.com.ilhasoft.ilhapay.model.Transaction;
import br.com.ilhasoft.ilhapay.network.CardHashResponse;
import br.com.ilhasoft.ilhapay.network.ErrorResponse;
import br.com.ilhasoft.ilhapay.network.PagarMeServices;
import br.com.ilhasoft.ilhapay.validator.CreditCardEncryptor;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by john-mac on 6/16/16.
 */
public class IlhaPay {

    private String apiKey;
    private String encryptionKey;

    private final Context context;
    private final PagarMeServices services;

    public IlhaPay(Context context) {
        this.context = context;
        this.services = new PagarMeServices();
    }

    public IlhaPay(Context context, String encryptionKey, String apiKey) {
        this(context);
        this.encryptionKey = encryptionKey;
        this.apiKey = apiKey;
    }

    public void sendTransaction(final Transaction transaction, final RegisterListener<Transaction> listener) {
        services.postTransaction(apiKey, transaction).enqueue(getDefaultRegisterCallback(listener
                , R.string.error_transaction));
    }

    public void sendTransactionRefund(final Integer transactionId, final RegisterListener<Transaction> listener) {
        services.postTransactionRefund(apiKey, transactionId).enqueue(getDefaultRegisterCallback(listener
                , R.string.error_transaction_refund));
    }

    public void updateRecipient(final Recipient recipient, final RegisterListener<Recipient> listener) {
        services.putRecipient(apiKey, recipient).enqueue(getDefaultRegisterCallback(listener
                , R.string.error_update_recipient));
    }

    public void registerRecipient(final Recipient recipient, final RegisterListener<Recipient> listener) {
        services.postRecipient(apiKey, recipient).enqueue(getDefaultRegisterCallback(listener
                , R.string.error_register_recipient));
    }

    public void registerCard(final CreditCard creditCard, final RegisterListener<SecureCreditCard> listener) {
        services.getCardHashKey(encryptionKey).enqueue(new Callback<CardHashResponse>() {
            @Override
            public void onResponse(Call<CardHashResponse> call, Response<CardHashResponse> response) {
                try {
                    CardHashResponse hashResponse = response.body();
                    String cardHash = hashResponse.getId() + "_" + CreditCardEncryptor.encrypt(creditCard, hashResponse.getPublicKey());

                    if (response.isSuccessful()) {
                        saveCardHash(cardHash, listener);
                    } else {
                        callErrorListener(response, listener);
                    }
                } catch(Exception exception) {
                    listener.onError(exception, context.getString(R.string.error_message_card_encryption));
                }
            }

            @Override
            public void onFailure(Call<CardHashResponse> call, Throwable throwable) {
                listener.onError(throwable, context.getString(R.string.error_message_card_encryption));
            }
        });
    }

    private void saveCardHash(String cardHash, final RegisterListener<SecureCreditCard> listener) {
        services.postCard(apiKey, cardHash).enqueue(getDefaultRegisterCallback(listener, R.string.error_message_card_saving));
    }

    @NonNull
    private <ResponseObject> Callback<ResponseObject> getDefaultRegisterCallback(final RegisterListener<ResponseObject> listener
            , @StringRes final int errorMesssageId) {
        return new Callback<ResponseObject>() {
            @Override
            public void onResponse(Call<ResponseObject> call, Response<ResponseObject> response) {
                try {
                    if (response.isSuccessful()) {
                        listener.onRegistered(response.body());
                    } else {
                        callErrorListener(response, listener);
                    }
                } catch (Exception exception) {
                    listener.onError(exception, context.getString(errorMesssageId));
                }
            }

            @Override
            public void onFailure(Call<ResponseObject> call, Throwable throwable) {
                listener.onError(throwable, context.getString(errorMesssageId));
            }
        };
    }

    private void callErrorListener(Response response, RegisterListener listener) throws Exception {
        Converter<ResponseBody, ErrorResponse> converter = createErrorConverter();
        ErrorResponse errorResponse = converter.convert(response.errorBody());

        String errorMessage = errorResponse.getErrors().get(0).getMessage();
        listener.onError(new IllegalStateException(errorMessage), errorMessage);
    }

    @NonNull
    @SuppressWarnings("unchecked")
    private Converter<ResponseBody, ErrorResponse> createErrorConverter() {
        return (Converter<ResponseBody, ErrorResponse>) GsonConverterFactory.create()
                .responseBodyConverter(ErrorResponse.class, ErrorResponse.class.getAnnotations(), services.getRetrofit());
    }

    public IlhaPay encryptionKey(String encryptionKey) {
        this.encryptionKey = encryptionKey;
        return this;
    }

    public IlhaPay apiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }
}
