package br.com.ilhasoft.ilhapay.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by john-mac on 6/21/16.
 */
public class Phone {

    @SerializedName("ddd")
    private String ddd;

    @SerializedName("number")
    private String number;

    public Phone(String ddd, String number) {
        this.ddd = ddd;
        this.number = number;
    }

    public Phone() {
    }

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
