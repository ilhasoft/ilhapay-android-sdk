package br.com.ilhasoft.ilhapay;

/**
 * Created by john-mac on 6/16/16.
 */
public interface RegisterListener<Type> {

    void onRegistered(Type value);

    void onError(Throwable throwable, String errorMessage);

}
