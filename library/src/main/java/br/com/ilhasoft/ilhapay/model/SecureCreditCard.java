package br.com.ilhasoft.ilhapay.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by john-mac on 6/16/16.
 */
public class SecureCreditCard {

    private String id;

    @SerializedName("date_created")
    private Date dateCreated;

    @SerializedName("date_updated")
    private Date dateUpdated;

    private String brand;

    @SerializedName("holder_name")
    private String holderName;

    @SerializedName("first_digits")
    private String firstDigits;

    @SerializedName("last_digits")
    private String lastDigits;

    private String country;

    private String fingerprint;

    private String customer;

    private Boolean valid;

    @SerializedName("expiration_date")
    private String expirationDate;

    public String getId() {
        return id;
    }

    public SecureCreditCard setId(String id) {
        this.id = id;
        return this;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public SecureCreditCard setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public SecureCreditCard setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
        return this;
    }

    public String getBrand() {
        return brand;
    }

    public SecureCreditCard setBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public String getHolderName() {
        return holderName;
    }

    public SecureCreditCard setHolderName(String holderName) {
        this.holderName = holderName;
        return this;
    }

    public String getFirstDigits() {
        return firstDigits;
    }

    public SecureCreditCard setFirstDigits(String firstDigits) {
        this.firstDigits = firstDigits;
        return this;
    }

    public String getLastDigits() {
        return lastDigits;
    }

    public SecureCreditCard setLastDigits(String lastDigits) {
        this.lastDigits = lastDigits;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public SecureCreditCard setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public SecureCreditCard setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
        return this;
    }

    public String getCustomer() {
        return customer;
    }

    public SecureCreditCard setCustomer(String customer) {
        this.customer = customer;
        return this;
    }

    public Boolean getValid() {
        return valid;
    }

    public SecureCreditCard setValid(Boolean valid) {
        this.valid = valid;
        return this;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public SecureCreditCard setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
        return this;
    }

    @Override
    public String toString() {
        return "SecureCreditCard{" +
                "id='" + id + '\'' +
                ", dateCreated=" + dateCreated +
                ", dateUpdated=" + dateUpdated +
                ", brand='" + brand + '\'' +
                ", holderName='" + holderName + '\'' +
                ", firstDigits='" + firstDigits + '\'' +
                ", lastDigits='" + lastDigits + '\'' +
                ", country='" + country + '\'' +
                ", fingerprint='" + fingerprint + '\'' +
                ", customer='" + customer + '\'' +
                ", valid=" + valid +
                ", expirationDate=" + expirationDate +
                '}';
    }
}
